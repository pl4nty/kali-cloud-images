import pytest

from debian_cloud_images.images.publicinfo import ImagePublicInfo, ImagePublicType
import re


def test_ImagePublicInfo():
    ImagePublicInfo()


def test_ImagePublicInfo_field():
    info = {
        'release_id': 'release',
        'arch': 'arch',
        'build_id': 'buildid',
        'version': 'version',
    }

    pdev = ImagePublicInfo(
        public_type=ImagePublicType.dev,
    ).apply(info)
    assert re.compile('[a-z]+-release-arch-dev-buildid').match(pdev.vendor_family) is not None
    assert re.compile('[a-z]+-release-arch-dev-buildid-version').match(pdev.vendor_name) is not None
    assert pdev.vendor_description

    pdaily = ImagePublicInfo(
        public_type=ImagePublicType.daily,
    ).apply(info)
    assert re.compile('[a-z]+-release-arch-daily').match(pdaily.vendor_family) is not None
    assert re.compile('[a-z]+-release-arch-daily-version').match(pdaily.vendor_name) is not None
    assert pdaily.vendor_description

    prelease = ImagePublicInfo(
        public_type=ImagePublicType.release,
    ).apply(info)
    assert re.compile('[a-z]+-release-arch').match(prelease.vendor_family) is not None
    assert re.compile('[a-z]+-release-arch-version').match(prelease.vendor_name) is not None
    assert prelease.vendor_description


def test_ImagePublicInfo_field_gce():
    len_max = 63
    len_release = 32
    len_arch = 32
    len_version = 8
    assert len_max < len_release + len_arch + len_version

    info = {
        'release_id': 'r' * len_release,
        'arch': 'a' * len_arch,
        'build_id': '',
        'version': 'v' * len_version,
    }

    p = ImagePublicInfo().apply(info)
    assert len(p.vendor_name63) == len_max
    assert len(p.vendor_gce_family) == len_max
    assert p.vendor_name63[-len_version:] == 'v' * len_version


def test_ImagePublicInfo_field_override():
    info = {
        'release_id': 'release',
        'arch': 'arch',
        'build_id': 'buildid',
        'version': 'version',
    }
    override_info = {
        'version': 'override',
    }

    pdev = ImagePublicInfo(
        public_type=ImagePublicType.dev,
        override_info=override_info,
    ).apply(info)
    assert re.compile('[a-z]+-release-arch-dev-buildid-override').match(pdev.vendor_name) is not None

    pdaily = ImagePublicInfo(
        public_type=ImagePublicType.daily,
        override_info=override_info,
    ).apply(info)
    assert re.compile('[a-z]+-release-arch-daily-override').match(pdaily.vendor_name) is not None

    prelease = ImagePublicInfo(
        public_type=ImagePublicType.release,
        override_info=override_info,
    ).apply(info)
    assert re.compile('[a-z]+-release-arch-override').match(prelease.vendor_name) is not None


def test_ImagePublicInfo_field_unknown():
    p = ImagePublicInfo().apply({})

    with pytest.raises(KeyError):
        p.undefined
